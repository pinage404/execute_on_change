#!/bin/bash
VERSION="0.1.0"

usage() {
	cat >&2 << EOF
execute_on_change $VERSION

DESCRIPTION:
	Execute a command on file (or directory) changes
	Can be use with docker and docker-compose

DEPENDANCIES:
	- 'bash'
	- 'getopt'
	- 'inotifywait'
		command provided by 'inotify-tools' on Debian

USAGE:
	execute_on_change [OPTIONS] [-- [file1 [file2 [... ]]]]
	
	OPTIONS:
		-h
		--help
			show this message
		
		-v
		--version
			show version of this tool
		
		       -c COMMAND
		    --cmd COMMAND
		--command COMMAND
			run COMMAND on change
		
		        --run-on-host
		--run-command-on-host
			run the command on host instead of in the docker container
		
		               -t TIMEOUT
		        --timeout TIMEOUT
		--command-timeout TIMEOUT
			timeout (in second) of the command before killed
			DEFAULT 0 to avoid this
		
		       --compose DOCKER_COMPOSE_SERVICE
		--docker-compose DOCKER_COMPOSE_SERVICE
			service to up and inside exec COMMAND
		
		 -b
		--build
			say to docker to build before exec
			this option implicitly add --restart
		
		                -u
		               --up
		       --compose-up
		--docker-compose-up
			restart service before execute COMMAND
		
		                -l    NUMBER_OF_LINE
		               --log  NUMBER_OF_LINE
		       --compose-log  NUMBER_OF_LINE
		--docker-compose-log  NUMBER_OF_LINE
		               --logs NUMBER_OF_LINE
		       --compose-logs NUMBER_OF_LINE
		--docker-compose-logs NUMBER_OF_LINE
			display NUMBER_OF_LINE of thedocker-compose-logs
			0 to avoid this
		
	ARGUMENTS:
		file
			file (or directory) to watch
			DEFAULT current directory
EOF
}



# utily function
warn() {
	echo "$@" >&2
}


die() {
	warn "$@"
	exit 1
}

kill_if_still_running() {
	# check if pid is still running before kill it
	if [ kill $1 > /dev/null 2>&1 ]; then
		kill $1
		return 0
	fi
	return 1
}


# default values
run_command_on_host=false
timeout=0
docker_compose_service=""
should_build=false
should_restart=false
log_tail="all"
file_to_watch="."

cmd() {
	warn "You should provide a COMMAND"
	die $(usage)
}



parse_arg() {
	# parse option
	opt=`getopt \
	--name $(basename $0) \
	--shell bash \
	--option hvc:t:bul: \
	--longoptions help,version,cmd:,command:,run-on-host,run-command-on-host,timeout:command-timeout:,compose:,docker-compose:,build,up,compose-up,docker-compose-up,log:,compose-log:,docker-compose-log:,logs:,compose-logs:,docker-compose-logs: \
	-- "$@"`
	
	# unknow argument
	if [ $? != 0 ]; then
		die $(usage)
	fi
	
	eval set -- "$opt"
	
	while true ; do
		case "$1" in
			-h | --help)
				usage
				exit 0
			;;
			-v | --version)
				echo "$VERSION"
				exit 0
			;;
			-c | --cmd | --command)
				cmd=$2
				shift 2
			;;
			--run-on-host | --run-command-on-host)
				run_command_on_host=true
				shift
			;;
			-t | --timeout | --command-timeout)
				timeout=$2
				shift 2
			;;
			--compose | --docker-compose)
				docker_compose_service=$2
				shift 2
			;;
			-b | --build)
				should_build=true
				should_restart=true
				shift
			;;
			-u | --up | --compose-up | --docker-compose-up)
				should_restart=true
				shift
			;;
			-l | --log | --compose-log | --docker-compose-log | --logs | --compose-logs | --docker-compose-logs)
				log_tail=$2
				shift 2
			;;
			--)
				shift
				break
			;;
			*)
				echo "unknown option: $1"
				break
			;;
		esac
	done
	
	if [ ! -z "$@" ]; then
		file_to_watch="$@"
	fi
}



run_command() {
	echo "$@"
	
	if [ "$timeout" -gt "0" ]; then
		# execute command in background
		pid=$($@ &)
		
		# wait for command
		# @TODO: improve this, should not wait the timeout if the process has exited
		sleep $timeout
		
		# check if pid is still running before kill it
		kill_if_still_running $pid
		
		wait
	else
		# execute command
		$@
	fi
	
	return $?
}


run_docker_compose() {
	return_status=1
	
	if [ "$should_restart" = true ]; then
		build=""
		if [ "$should_build" = true ]; then
			build="--build"
		fi
		
		# start service
		docker-compose up $build -d $docker_compose_service
	fi
	
	
	if [[ "$log_tail" == "all" || "$log_tail" -ne "0" ]]; then
		# show logs of the service
		docker-compose logs --follow --timestamps --tail $log_tail $docker_compose_service &
	fi
	
	# run command
	_cmd="$cmd"
	if [ "$run_command_on_host" != true ]; then
		_cmd="docker-compose exec -T $docker_compose_service $cmd"
	fi
	run_command "$_cmd"
	return_status=$?
	
	# add new line
	echo ""
	
	# stop container
	if [ "$should_restart" = true ]; then
		docker-compose stop $docker_compose_service
	fi
	
	return $return_status
}


run() {
	clear
	
	if [ ! -z "$docker_compose_service" ]; then
		run_docker_compose
	else
		run_command "$cmd"
	fi
	
	return $?
}


main() {
	# check dependancies
	if ! hash inotifywait 2>/dev/null; then
		die "You need \"inotifywait\" to be installed"
	fi
	
	if ! hash getopt 2>/dev/null; then
		die "You need \"getopt\" to be installed"
	fi
	
	# parse arguments
	parse_arg "$@"
	
	# run once at start
	run
	
	echo "Press CTRL + C to stop ; Watching files..."
	# at each save in the folder (recursively) execute the command
	inotifywait \
		--monitor \
		--event modify \
		--event close_write \
		--recursive \
		$file_to_watch |
	while read -r directory event filename ; do
		run
		echo "Press CTRL + C to stop ; Watching files..."
	done
}



main "$@"
